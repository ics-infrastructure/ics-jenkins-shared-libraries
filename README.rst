ESS ICS Jenkins shared libraries
================================

This repository includes Jenkins_ shared libraries used by ICS.

.. _Jenkins: https://jenkins.io/doc/book/pipeline/shared-libraries/
