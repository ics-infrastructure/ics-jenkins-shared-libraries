package se.esss.jenkins

class UpdateCommitStatus {

    static def gitlabStatus(script, state, repoSlug, owner, sha, token) {
        // update the build status of a gitlab commit
        String projectPath = owner + '%2F' + repoSlug
        String base = "https://gitlab.esss.lu.se/api/v4/projects/${projectPath}/statuses/${sha}"
        def params = [state: state, name: 'Jenkins', target_url: "${script.JOB_URL}${script.BUILD_NUMBER}"]
        String url = base + '?' + params.collect { k,v -> "$k=$v" }.join('&')
        script.sh "curl -X POST --header \"PRIVATE-TOKEN: $token\" \"${url}\" || true"
    }

     static def bitbucketStatus(script, state, repoSlug, owner, sha, username, password) {
        // update the build status of a bitbucket commit
         String url = "https://api.bitbucket.org/2.0/repositories/${owner}/${repoSlug}/commit/${sha}/statuses/build"
         def params = [state: state, key: "JENKINS-BUILD-${script.BUILD_NUMBER}", url: "${script.JOB_URL}${script.BUILD_NUMBER}", name: 'Jenkins']
         params = params.collect { k,v -> "$k=$v" }.join('&')
         script.sh "curl -X POST --user \"${username}:${password}\" -d \"${params}\" \"${url}\" || true"
     }
}
