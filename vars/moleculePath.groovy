def call(baseDir) {
    def dirFiles = sh(script: "ls $baseDir", returnStdout: true)
    if (dirFiles.contains('molecule.yml')) {
        println "molecule v1 detected"
        return '/opt/conda/envs/molecule/bin'
    }
    if (dirFiles.contains('molecule')) {
        println "molecule v2 detected"
        return '/opt/conda/envs/molecule2/bin'
    }
    println "No molecule detected"
    return ''
}
