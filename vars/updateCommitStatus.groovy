import static se.esss.jenkins.UpdateCommitStatus.*

def call(String state, String repoUrl, String sha) {
    // map to convert bitbucket state to gitlab state
    def GITLAB_STATUS = [
        INPROGRESS: 'running',
        SUCCESSFUL: 'success',
        CANCELED: 'canceled',
        FAILED: 'failed'
        ]
    String repoSlug = repoUrl.split('/').last().replace('.git', '')
    String owner = repoUrl.split('/')[-2]
    if (repoUrl.contains('gitlab.esss.lu.se')) {
        withCredentials([string(
            credentialsId: 'gitlab-token',
            variable: 'gitlabToken'
        )]) {
            gitlabStatus(this, GITLAB_STATUS[state], repoSlug, owner, sha, gitlabToken)
        }
    } else if (repoUrl.contains('bitbucket.org')) {
         withCredentials([usernamePassword(
             credentialsId: 'benjaminbertrand-noemail-bitbucket',
             usernameVariable: 'username',
             passwordVariable: 'password'
         )]) {
            bitbucketStatus(this, state, repoSlug, owner, sha, username, password)
         }
    }
}
